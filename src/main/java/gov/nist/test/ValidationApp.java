package gov.nist.test;

import gov.nist.validation.report.Report;
import hl7.v2.profile.Profile;
import hl7.v2.profile.XMLDeserializer;
import hl7.v2.validation.SyncHL7Validator;
import hl7.v2.validation.content.ConformanceContext;
import hl7.v2.validation.content.DefaultConformanceContext;
import hl7.v2.validation.vs.ValueSetLibrary;
import hl7.v2.validation.vs.ValueSetLibraryImpl;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

public class ValidationApp {
	
	/**
	  * Shows how to invoke the validation and navigate the result 
	  */
	public static void main( String[] args ) {
		try {
			
			Profile profile = getProfile();
			
			ConformanceContext c = getConformanceContext();

			ValueSetLibrary valueSetLibrary = getValueSetLibrary();
			
			// An instance of the message validator
			// This should be a singleton for a specific tool. We create it once and reuse it across validations
			
			SyncHL7Validator validator = new SyncHL7Validator(profile, valueSetLibrary, c);
			
			// The message instance (ER7)
			String message = Util.streamAsString("/message.er7");

			// The check method will throw an exception if something goes wrong
			Report report = validator.check(message, "ORU_R01");

			System.out.println( report.toText() );
			
			System.out.println( "======= Report as Json \n" );
			System.out.println( report.toJson() );
			System.out.println( "\n\n" );

		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * @return An instance of the message profile
	 */
	static Profile getProfile() {
		// The profile in XML
		InputStream profileXML = ValidationApp.class.getResourceAsStream("/Profile.xml");
					
		// The get() at the end will throw an exception if something goes wrong
		Profile profile = XMLDeserializer.deserialize(profileXML).get();
		
		return profile;
	}
	
	/**
	 * @return An instance of the conformance context
	 */
	static ConformanceContext getConformanceContext() {
		// The first conformance context XML file
		InputStream contextXML1 = ValidationApp.class.getResourceAsStream("/Constraints.xml");

		// The second conformance context XML file
		InputStream contextXML2 = ValidationApp.class.getResourceAsStream("/Predicates.xml");

		List<InputStream> confContexts = Arrays.asList( contextXML1, contextXML2 );

		// The get() at the end will throw an exception if something goes wrong
		ConformanceContext c = DefaultConformanceContext.apply(confContexts).get();
		return c;
	}
	
	/**
	 * @return An instance of the value set library
	 */
	static ValueSetLibrary getValueSetLibrary() {
		InputStream vsLibXML = ValidationApp.class.getResourceAsStream("/ValueSets.xml");
		
		ValueSetLibrary valueSetLibrary = ValueSetLibraryImpl.apply(vsLibXML).get();
		
		return valueSetLibrary;
	}

}
