package gov.nist.test.plugin;

import hl7.v2.instance.Element;

/**
 * This is dummy plugin that can be called within a constraint
 */
public class Dummy {
	
	/**
	 * @return True if the asssertion succeed, false otherwise
	 */
	public boolean assertion(Element context) {
		return false;
	}

}
