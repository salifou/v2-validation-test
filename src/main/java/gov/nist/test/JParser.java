package gov.nist.test;

import scala.util.Try;
import hl7.v2.instance.Message;
import hl7.v2.parser.impl.DefaultParser;
import hl7.v2.parser.impl.DefaultParser$class;

public class JParser implements DefaultParser {

	/**
	  * A java friendly way to call the `parse` method of the default parser implementation
	  * @param message - The message as a string
	  * @param model   - The message model
	  * @return The message instance model encapsulated in a scala `scala.util.Try`
	  */
	@SuppressWarnings("unchecked")
	public Try<Message> parse(String message, hl7.v2.profile.Message model) {
		return (Try<Message>) DefaultParser$class.parse( this, message, model);
	}
	
	
	/**
	  * Call JParser.parse method and decapsulate the result
	  */
	public Message jparse(String message, hl7.v2.profile.Message model) throws Exception {
		return (Message) parse(message, model).get();
	}

}
