package gov.nist.test;

import hl7.v2.instance.Complex;
import hl7.v2.instance.Element;
import hl7.v2.instance.Group;
import hl7.v2.instance.Line;
import hl7.v2.instance.Message;
import hl7.v2.instance.Simple;
import hl7.v2.profile.Profile;
import hl7.v2.profile.Range;
import hl7.v2.profile.Usage;
import hl7.v2.profile.XMLDeserializer;

import java.io.InputStream;

public class ParserApp {

	/**
	  * Shows how to invoke the parser and navigate the result 
	  */
	public static void main(String[] args) {
		try {
			
			// The profile in XML
			InputStream profileXML = ParserApp.class.getResourceAsStream("/Profile.xml");

			// The get() at the end will throw an exception if something goes wrong
			Profile profile = XMLDeserializer.deserialize(profileXML).get();

			// The message instance (ER7)
			String message = Util.streamAsString("/message.er7");
			
			JParser p = new JParser();
			
			Message m = p.jparse( message, profile.messages().apply("ORU_R01") );
			
			//scala.collection.immutable.List<Tuple2<Integer, String>> i = m.invalid();
			
			// Print the message as String
			System.out.println( asString(m) );
			
			System.out.println("Invalid Lines:");
			System.out.println( badLinesAsString( m.invalid() ) );
			
			System.out.println("Unexpected Lines:");
			System.out.println( badLinesAsString( m.unexpected() ) );
			
		} catch(Exception e){
			e.printStackTrace();
		}
	}
	
	/**
	  * Convert the message instance model to string 
	  */
	private static String asString(Message m) {
		StringBuilder sb = new StringBuilder();
		Group g = m.asGroup();
		scala.collection.immutable.List<Element> children = ( (Complex) g).children();
		scala.collection.Iterator<Element> it = children.iterator();
		while( it.hasNext() ) {
			sb = sb.append( asString( it.next() ) ).append("\n");
		}
		return sb.toString();
	}
	
	/**
	  * Recursively traverse the element and convert it to String 
	  */
	private static String asString(Element e) {
		int line = e.location().line();
		String path = e.location().path();
		String desc = e.location().desc();
		Usage usage = e.req().usage(); 
		Range cardinality = Util.getOption( e.req().cardinality() );
		//Range length      = Util.getOption( e.req().length() );
		//String confLength = Util.getOption( e.req().confLength() );
		//String table      = Util.getOption( e.req().table() );
		
		StringBuilder sb = new StringBuilder();
		sb = sb.append("#"+line+"  ");
		sb = sb.append( path ).append(": ").append(desc).append(" ").append(usage);
		if( cardinality != null )
			sb = sb.append("[").append( cardinality.min() ).append("..").append(cardinality.max()).append("]");
		
		// Get the value of the simple element
		if( e instanceof Simple )
			sb = sb.append( " # Value = '" ).append( ((Simple) e).value().raw() ).append("'");
		// Get the children of the complex element
		else {
			scala.collection.Iterator<Element> it = ((Complex) e).children().iterator();
			while( it.hasNext() ) {
				sb = sb.append("\n\t").append( asString( it.next() ) );
			}
		}
		return sb.toString();
	}
	
	/**
	  * Convert the list of Lines to String
	  */
	private static String badLinesAsString( scala.collection.immutable.List<Line> list ) {
		StringBuilder sb = new StringBuilder();
		scala.collection.Iterator<Line> it = list.iterator();
		while(it.hasNext()) {
			Line l = it.next();
			sb = sb.append("\t[Line=").append(l.number()).append(", column=1] ").append(l.content()+"\n");
		}
		return sb.toString();
	}

}
